import 'package:practice_dart/practice_dart.dart' as practice_dart;

void main(List<String> arguments) {
  final List<int> i = [1, 2, 3, 4, 10];
  // listMap(i);
  // forLoop(i);
  // whileLoop(i);
  // doWhileLopp(i);
  // forEachLoop(i);
  forInLoop(i);
  print(i);
}

//Work with every object in a list, then return IterableMap, to return list,
//we call method toList().
void listMap(List z) {
  z = z.map((e) {
    return e += 1;
  }).toList();
}

//Same as forEach
void forInLoop(List z) {
  int i = 0;
  for (int el in z) {
    el = 1;
    print(el);
    z[i] = el;
    i++;
  }
}

//ForEach this is the void function that apply function to every element in the list
void forEachLoop(List z) {
  int i = 0;
  z.forEach((element) {
    element = 2;
    print(element);
    z[i] = element;
    i++;
  });
}

//For is loop method that has 3 properties: variable, condition and function.
//At first check condition and if condition true executes
//the code, until condition will not false.
void forLoop(List z) {
  for (var i = 0; i < z.length; i++) {
    z[i] = 47;
  }
}

//While is same as for, but it's have only condition.
void whileLoop(List z) {
  int i = 0;
  while (z.contains(10)) {
    z[i] = 0;
    i++;
  }
}

//do while is same as while, but it will execute the code at least 1 time.
void doWhileLopp(List z) {
  int i = 0;
  do {
    z[i] = 0;
    i++;
  } while (z.length > 10);
}
